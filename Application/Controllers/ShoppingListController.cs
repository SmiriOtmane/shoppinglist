﻿using Application.Areas.Identity.Data;
using Application.IRepository;
using Application.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Application.Controllers
{
    [Authorize]
    public class ShoppingListController : Controller
    {
        private readonly UserManager<ApplicationUser> _manager;
        private readonly IUnitOfWork _unitOfWork;

        public ShoppingListController(IUnitOfWork unitOfWork, UserManager<ApplicationUser> manager)
        {
            _manager = manager;
            _unitOfWork = unitOfWork;
        }

        // GET: ShoppingList
        public IActionResult Index()
        {
            IEnumerable<ShoppingList> shoppingLists = _unitOfWork.ShoppingList.GetAll();
            return View(shoppingLists);
        }

        // GET: ShoppingList/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null || _unitOfWork.ShoppingList == null)
            {
                return NotFound();
            }

            var shoppingList =  _unitOfWork.ShoppingList.GetFirstOrDefault(m => m.Id == id);

            if (shoppingList == null)
            {
                return NotFound();
            }

            return View(shoppingList);
        }

        // GET: ShoppingList/Create
        public IActionResult Create()
        {
            return View();
        }

       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,ShoppingListName,ShoppingListArticles,ListReserved")] ShoppingList shoppingList)
        {
            ApplicationUser applicationUser = await _manager.GetUserAsync(User);
            shoppingList.ApplicationUser = applicationUser;
            shoppingList.DateCreated = DateTime.Now;
            ModelState.Remove("ApplicationUser");
            if (ModelState.IsValid)
            {
                _unitOfWork.ShoppingList.Add(shoppingList);
                _unitOfWork.Save();
                return RedirectToAction(nameof(Index));
            }
            return View(shoppingList);
        }

        // GET: ShoppingList/Edit/x
        public IActionResult Edit(int? id)
        {
            if (id == null || _unitOfWork.ShoppingList == null)
            {
                return NotFound();
            }

            var shoppingList = _unitOfWork.ShoppingList.GetFirstOrDefault(s=>s.Id == id);

            if (shoppingList == null)
            {
                return NotFound();
            }
            return View(shoppingList);
        }

        // POST: ShoppingList/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,ShoppingListName,ShoppingListArticles,ListReserved,DateCreated")] ShoppingList shoppingList)
        {
            if (id != shoppingList.Id)
            {
                return NotFound();
            }

            ApplicationUser applicationUser = await _manager.GetUserAsync(User);
            shoppingList.ApplicationUser = applicationUser;
            ModelState.Remove("ApplicationUser");

            if (ModelState.IsValid)
            {
                try
                {
                    _unitOfWork.ShoppingList.Update(shoppingList);
                    _unitOfWork.Save();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ShoppingListExists(shoppingList.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(shoppingList);
        }

        // GET: ShoppingList/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _unitOfWork.ShoppingList == null)
            {
                return NotFound();
            }

            var shoppingList = _unitOfWork.ShoppingList.GetFirstOrDefault(s => s.Id == id);

            if (shoppingList == null)
            {
                return NotFound();
            }

            return View(shoppingList);
        }

        // POST: ShoppingList/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_unitOfWork.ShoppingList == null)
            {
                return Problem("Entity set 'ApplicationDbContext.ShoppingList'  is null.");
            }
            var shoppingList = _unitOfWork.ShoppingList.GetFirstOrDefault(s => s.Id == id);
            if (shoppingList != null)
            {
                _unitOfWork.ShoppingList.Remove(shoppingList);
            }
            
            _unitOfWork.Save();
            return RedirectToAction(nameof(Index));
        }

        private bool ShoppingListExists(int id)
        {
          return _unitOfWork.ShoppingList.GetAll().Any(e => e.Id == id);
        }
    }
}
