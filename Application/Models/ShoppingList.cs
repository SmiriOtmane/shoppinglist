﻿using Application.Areas.Identity.Data;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace Application.Models
{
    public class ShoppingList
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [Display(Name = "List Name")]
        public string ShoppingListName { get; set; }
        [Required]
        [Display(Name = "List Of Items")]
        public string ShoppingListArticles { get; set; }
        public DateTime? ListReserved { get; set; }
        public DateTime? DateCreated { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
