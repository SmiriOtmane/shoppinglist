﻿using Application.Models;

namespace Application.IRepository
{
    public interface IShoppingListRepository : IRepository<ShoppingList>
    {
        void Save();
        void Update(ShoppingList shoppingList);
    }
}
