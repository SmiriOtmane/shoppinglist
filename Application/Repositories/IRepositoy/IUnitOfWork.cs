﻿namespace Application.IRepository
{
    public interface IUnitOfWork
    {
        IShoppingListRepository ShoppingList { get; }
        void Save();
    }
}
