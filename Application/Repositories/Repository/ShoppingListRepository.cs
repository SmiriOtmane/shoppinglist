﻿using Application.Data;
using Application.IRepository;
using Application.Models;
using Application.Repository;

namespace Application.DataAccess.Repository
{
    public class ShoppingListRepository : Repository<ShoppingList> , IShoppingListRepository
    {
        private readonly ApplicationDbContext _db;

        public ShoppingListRepository(ApplicationDbContext db) :base(db)
        {
            _db = db;
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public void Update(ShoppingList shoppingList)
        {
            _db.Update(shoppingList);
        }
    }
}
