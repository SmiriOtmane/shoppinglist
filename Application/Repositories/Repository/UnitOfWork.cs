﻿using Application.Data;
using Application.IRepository;

namespace Application.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _db;
        public IShoppingListRepository ShoppingList { get; private set; }
        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            ShoppingList = new ShoppingListRepository(_db);
        }
        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
